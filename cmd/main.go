package main

import (
	s "codeberg.org/Developer1214/3d-Cacl/server"
	dir "codeberg.org/Developer1214/3d-Cacl/internal/pkg/dir"
)

func main() {
	dir.DirForFiles()
	s.RunApp("localhost:3001")
}
