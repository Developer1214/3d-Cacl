package stl

import (
	stl "github.com/hschendel/stl"
	file "codeberg.org/Developer1214/3d-Cacl/internal/pkg/file"
)

type FileStl struct {
	Data *stl.Solid `json:"-"`
	File *file.File
}

func NewFileStl(path string) (*FileStl,error){
	data, err  := stl.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return &FileStl{
		Data: data,
		File: &file.File{
			Material: "",
			Path: "",
			CalcData: new(file.CalcData),
		},
	}, nil
}
