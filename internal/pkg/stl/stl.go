package stl

import (
	"math"

	ownMath "codeberg.org/Developer1214/3d-Cacl/internal/pkg/math"
)

//Function for use in handleFunc
func Result(path string) (interface{}, error) {

	//Create new file on stack
	s, err  := NewFileStl(path)
	if err != nil {
		return nil, err
	}

	s.File.CalcData.Surface = ownMath.ToFixed32(s.clacSurface(), 2)
	s.File.CalcData.Volume = ownMath.ToFixed32(s.calcVolume(), 2)
	s.File.CalcData.Bbox = ownMath.ToFixed32(s.calcBbox(), 2)
	s.File.CalcData.Price = 0
	s.File.CalcData.Size = s.calceSize()
	s.File.CalcData.Parts = s.calcParts()

	return s.File.CalcData, nil
}

func (s *FileStl) calceSize() [3]float32 {
	len1 := s.Data.Measure().Len[0]
	len2 := s.Data.Measure().Len[1]
	len3 := s.Data.Measure().Len[2]
	return [3]float32{ownMath.ToFixed32(len1, 2), ownMath.ToFixed32(len2, 2), ownMath.ToFixed32(len3, 2)}
}

func (s *FileStl) calcVolume() float32 {
	var volume float32
	for _, el := range s.Data.Triangles {
		volume += ownMath.SignedVolumeOfTriangle(el.Vertices[0], el.Vertices[1], el.Vertices[2])
	}
	return volume
}

func (s *FileStl) calcParts() int {
	parts := len(s.Data.Triangles)
	return parts
}

//Calca surface in см2 (/100) at the and
func (s *FileStl) clacSurface() float32 {
	var surface float64
	for _, el := range s.Data.Triangles {
		ax := el.Vertices[1][0] - el.Vertices[0][0]
		ay := el.Vertices[1][1] - el.Vertices[0][1]
		az := el.Vertices[1][2] - el.Vertices[0][2]
		bx := el.Vertices[2][0] - el.Vertices[0][0]
		by := el.Vertices[2][1] - el.Vertices[0][1]
		bz := el.Vertices[2][2] - el.Vertices[0][2]
		cx := ay*bz - az*by
		cy := az*bx - ax*bz
		cz := ax*by - ay*bx
		surface += 0.5 * math.Sqrt(float64(cx)*float64(cx)+float64(cy)*float64(cy)+float64(cz)*float64(cz))
	}
	return float32(surface / 100)
}

//Return BBox val in cm3
func (s *FileStl) calcBbox() float32 {
	len1 := s.Data.Measure().Len[0]
	len2 := s.Data.Measure().Len[1]
	len3 := s.Data.Measure().Len[2]
	return (len1 * len2 * len3) / 1000

}
