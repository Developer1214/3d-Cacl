package obj

import (
	file "codeberg.org/Developer1214/3d-Cacl/internal/pkg/file"
	dec "github.com/g3n/engine/loader/obj"
)

type FileObj struct {
	Data *dec.Decoder `json:"Data"`
	File *file.File
}

func NewFileObj(path string) (*FileObj, error) {
	data, err := dec.Decode(path, "data.mtl")
	if err != nil {

		return nil, err
	}
	return &FileObj{
		Data: data,
		File: &file.File{
			Material: "",
			Path:     "",
			CalcData: new(file.CalcData),
		},
	}, nil
}
