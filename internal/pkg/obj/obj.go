package obj

func Result(path string) (interface{}, error) {

	//Create new obj file in stack
	o, err := NewFileObj(path)
	if err != nil {
		return nil, err
	}

	return o, nil
}

func (o *FileObj) calcVolume() float32 {
	return 0
}
