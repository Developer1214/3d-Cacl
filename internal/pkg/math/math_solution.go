package math

import "math"

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func ToFixed64(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

func ToFixed32(num float32, precision int) float32 {
	output := math.Pow(10, float64(precision))
	return float32(round(float64(num)*output)) / float32(output)
}

//Return Volume of triangle in cm3("/1000" at the end)
func SignedVolumeOfTriangle(p1, p2, p3 [3]float32) float32 {
	v321 := p3[0] * p2[1] * p1[2]
	v231 := p2[0] * p3[1] * p1[2]
	v312 := p3[0] * p1[1] * p2[2]
	v132 := p1[0] * p3[1] * p2[2]
	v213 := p2[0] * p1[1] * p3[2]
	v123 := p1[0] * p2[1] * p3[2]
	return ((1.0 / 6.0) * (-v321 + v231 + v312 - v132 - v213 + v123)) / 1000
}
