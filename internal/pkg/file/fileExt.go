package file

import "errors"

func CheckExt(filaname string) (string,error) {
	extAllowed := []string{".obj", ".stl", ".STL", ".OBJ"}
	str := filaname[len([]rune(filaname))-4:]
	for _, el := range extAllowed {
		if str == el {

			return el, nil
		}
	}
	err := errors.New("Bad extanation from file: " + filaname)
	return "", err
}