package file

type CalcData struct {
	Surface float32    `json:"Surface"`
	Volume  float32    `json:"Volume"`
	Bbox    float32    `json:"Bbox"`
	Price   float32    `json:"Price"`
	Size    [3]float32 `json:"Size"`
	Parts   int        `json:"Parts"`
}

type File struct {
	Material string    `json:"-"`
	Path     string    `json:"-"`
	CalcData *CalcData `json:"Data"`
}
