package dir

import "os"

type DirForUploads struct {}

func(d *DirForUploads) checkDirExist() bool {
	_ ,err := os.Stat("uploads/")
	if err != nil {
		return os.IsExist(err)
		

	}
	return true
}

func(d *DirForUploads) createDir() {
	os.Mkdir("uploads/",os.ModePerm)
}


func DirForFiles() {
	dir := new(DirForUploads)
	if(!dir.checkDirExist()){
		dir.createDir()
	}
}

