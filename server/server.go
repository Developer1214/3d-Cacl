package server

import (
	"io/ioutil"
	"net/http"
	"os"

	file "codeberg.org/Developer1214/3d-Cacl/internal/pkg/file"
	"codeberg.org/Developer1214/3d-Cacl/internal/pkg/obj"
	stl "codeberg.org/Developer1214/3d-Cacl/internal/pkg/stl"
	gin "github.com/gin-gonic/gin"
)

func HandleError(c *gin.Context, err error) {
	c.JSON(http.StatusInternalServerError, "Error"+ err.Error())
}

func UploadFile(c *gin.Context) {

	// Detect if file more then 512MB
	err := c.Request.ParseMultipartForm(128 << 20)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "Big size file, need less then 128MB")
		return
	}

	//Read header from file
	_, header, err := c.Request.FormFile("file")

	if err != nil {
		HandleError(c,err)
		return
	}

	//file ext from filename
	ext, err := file.CheckExt(header.Filename)
	if err != nil {
		HandleError(c,err)
		return
	}

	//Create temp file on the server
	fileTmp, err := ioutil.TempFile("uploads/", "upload-*"+header.Filename)

	if err != nil {
		HandleError(c,err)
		return
	}
	defer func() {
		fileTmp.Close()
		os.Remove(fileTmp.Name())
	}()

	//Save file in server dir
	c.SaveUploadedFile(header, fileTmp.Name())

	//Switch between two formats
	switch ext {

	case ".obj", ".OBJ":
		data, err := obj.Result(fileTmp.Name())
		if err != nil || data == nil {
			HandleError(c,err)
			return
		}
		c.JSON(http.StatusOK, &data)

	case ".stl", ".STL":
		//Get data from file and return it to rect
		data, err := stl.Result(fileTmp.Name())

		if err != nil || data == nil {
			HandleError(c,err)

			return
		}
		c.JSON(http.StatusOK, &data)
	default:
		c.JSON(http.StatusInternalServerError, "error"+err.Error()) //Потом отсавить только Error
	}

}

func RunApp(port string) {
	//Create gin object
	r := gin.Default()
	//Create config object
	config := ConfigServer()
	//Use config
	r.Use(config)
	//Allowed req
	r.POST("/", UploadFile)
	//Function which run server
	r.Run(port)

}
