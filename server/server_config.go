package server

import (
	"time"

	cors "github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func ConfigServer() gin.HandlerFunc {

	conf := cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:3000", "https://localhost:3000"},
		AllowMethods:     []string{"POST"},
		AllowHeaders:     []string{"Origin", "Content-Type", "Content-Lenght"},
		MaxAge:           50 * time.Second,
		AllowCredentials: false,
		AllowFiles:       false,
	})
	return conf
}
